const router = require('express').Router();
const { to } = require('await-to-js');
const MemoryGame = require('../models/memory-game');
const STATUS_CODES = require('../utils/status-codes');
const dbErr = require('../utils/db-err');
const { memoryGame, flipCard } = require('../utils/memory-game');

// new game
router.post('/', async (req, res, next) => {
  const board = memoryGame();
  const game = new MemoryGame({ board });
  const [err, obj] = await to(game.save());
  const e = dbErr(err, obj); if (e) return next(e);

  res.status(STATUS_CODES.CREATED);
  return res.send({ id: obj.id, board });
});

// get game board
router.get('/:id', async (req, res, next) => {
  const [err, game] = await to(MemoryGame.findById(req.params.id));
  const e = dbErr(err, game); if (e) return next(e);

  return res.send({ id: game.id, board: game.board });
});

// flip Card
router.put('/:id', async (req, res, next) => {
  // TODO: out of bounds, sanitize input
  const [err, game] = await to(MemoryGame.findById(req.params.id));
  const e = dbErr(err, game); if (e) return next(e);

  const { index } = req.body;
  const { board } = game;
  const filpState = flipCard(index, board);

  const [errObj, obj] = await to(MemoryGame.updateOne({ _id: req.params.id }, { board }));
  const eObj = dbErr(errObj, obj); if (eObj) return next(eObj);

  return res.send({ id: game.id, board: game.board, state: filpState });
});

module.exports = router;
