const mongoose = require('mongoose');

const connect = () => {
  mongoose.connect(process.env.DB, { useNewUrlParser: true })
    .then(() => {
      console.debug('Database connection successful');
    })
    .catch((err) => {
      console.error(`Database connection error - ${err}`);
    });

  const db = mongoose.connection;
};

connect();

module.exports = mongoose;
