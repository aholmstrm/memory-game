const createError = require('http-errors');
const STATUS_CODES = require('../utils/status-codes');

// catch 404 and forward to error handler
const notFoundHandler = ((req, res, next) => {
  next(createError(STATUS_CODES.NOT_FOUND));
});

module.exports = notFoundHandler;
