const STATUS_CODES = require('../utils/status-codes');

// error handler
// eslint-disable-next-line no-unused-vars
const errorHandler = ((err, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  console.error(`${err.status || STATUS_CODES.INTERNAL_SERVER_ERROR} - ${err.message} - ${req.originalUrl} - ${req.method} - ${req.ip}`);

  // render the error page
  res.status(err.status || STATUS_CODES.INTERNAL_SERVER_ERROR);
  res.send({ error: err });
});

module.exports = errorHandler;
