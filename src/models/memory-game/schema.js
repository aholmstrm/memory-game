const mongoose = require('mongoose');

const schema = new mongoose.Schema({
  board: {
    type: [{}],
    required: [true],
  },
  createdAt: {
    type: Date,
    required: [true],
    default: Date.now,
  },
});

module.exports = schema;
