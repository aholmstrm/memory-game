const mongoose = require('../../db');
const schema = require('./schema');

const MemoryGame = mongoose.model('MemoryGame', schema);

module.exports = MemoryGame;
