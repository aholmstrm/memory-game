const createError = require('http-errors');
const STATUS_CODES = require('./status-codes');

const dbErr = ((err, obj) => {
  if (err) return createError(STATUS_CODES.INTERNAL_SERVER_ERROR);
  if (!obj || obj.n < 1) return createError(STATUS_CODES.NOT_FOUND);
  return null;
});

module.exports = dbErr;
