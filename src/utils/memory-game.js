const cardState = {
  available: 'available',
  unavailable: 'unavailable',
  fliped: 'fliped',
};

const flipState = {
  next: 'next',
  unavailable: 'unavailable',
  success: 'success',
  bad: 'bad',
  end: 'end',
};

// from: https://gist.github.com/guilhermepontes/17ae0cc71fa2b13ea8c20c94c5c35dc4
const shuffleArray = ((arr) => {
  return arr.map(a => [Math.random(), a])
    .sort((a, b) => a[0] - b[0])
    .map(a => a[1]);
});

const getGameCardPair = ((num) => {
  return Array(num).fill().map((_, i) => ({ image: `card${i}`, state: cardState.available }));
});

const deepCopy = ((gameCards) => {
  return gameCards.reduce((result, current) => {
    // dirty deep copy of object
    result.push(JSON.parse(JSON.stringify(current)));
    return result;
  }, []);
});

const newGameBoard = ((gameCards) => {
  const deep = deepCopy(gameCards);
  let cards = gameCards.concat(deep);
  cards = shuffleArray(cards);
  return cards;
});

const setState = ((state, gameBoard) => {
  return gameBoard.forEach((element) => {
    const e = element;
    if (element.state === cardState.fliped) e.state = state;
  });
});

const gameEnd = ((gameBoard) => {
  return gameBoard.reduce((result, current) => {
    if (current.state === cardState.available) return false;
    return result;
  }, true);
});

const flipNext = ((gameBoard) => {
  const flips = gameBoard.reduce((result, current) => {
    if (current.state === cardState.fliped) return result + 1;
    return result;
  }, 0);

  return flips === 1;
});

const flipValid = ((gameBoard) => {
  const flips = gameBoard.reduce((result, current) => {
    const cards = result;
    if (current.state === cardState.fliped) cards.push(current);
    return result;
  }, []);

  return flips[0].image === flips[1].image;
});

const flipCard = ((index, gameBoard) => {
  const board = gameBoard;
  if (board[index].state === cardState.fliped || board[index].state === cardState.unavailable) {
    return flipState.unavailable;
  }
  board[index].state = cardState.fliped;

  if (gameEnd(gameBoard)) return flipState.end;
  if (flipNext(gameBoard)) return flipState.next;
  const valid = flipValid(gameBoard) ? flipState.success : flipState.bad;
  if (valid === flipState.bad) setState(cardState.available, gameBoard);
  if (valid === flipState.success) setState(cardState.unavailable, gameBoard);

  return valid;
});

const memoryGame = (() => {
  return newGameBoard(getGameCardPair(3));
});

module.exports = {
  memoryGame,
  flipCard,
  cardState,
  flipState,
};
