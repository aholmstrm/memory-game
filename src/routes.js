const express = require('express');

const router = express.Router();

const memoryGame = require('./controllers/memory-games');
router.use('/api/games', memoryGame);

module.exports = router;
