const express = require('express');
const logger = require('morgan');
const helmet = require('helmet');
const cors = require('cors');
require('dotenv').config();

const routes = require('./src/routes');
const notFoundHandler = require('./src/middleware/not-found-handler');
const errorHandler = require('./src/middleware/error-handler');

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(helmet());
app.use(cors());

app.use(routes);
app.use(notFoundHandler);
app.use(errorHandler);

module.exports = app;
