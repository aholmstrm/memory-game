FROM node:10-alpine

MAINTAINER Anders Holmström

COPY . /var/www
WORKDIR /var/www

RUN npm install

EXPOSE $PORT

ENTRYPOINT ["npm", "start"]
