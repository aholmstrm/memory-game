# Memory game REST Api

## New game
$ curl -i -X POST  http://localhost:8080/api/games

## Show game board
$ curl -i http://localhost:8080/api/games/<id>

## Flip card
$ curl -i -X PUT -H "Content-Type: application/json" -d '{"index":"1"}' http://localhost:8080/api/games/<id>

# Docker

$ sudo docker-compose build

$ npm run docker ($ sudo docker-compose up)
